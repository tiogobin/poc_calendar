from datetime import datetime, timedelta
import sqlite3

con = sqlite3.connect("planning.db")
cur = con.cursor()

#create database file
#cur.execute("CREATE TABLE planning(day, description, start_time, end_time, creation_date)")

days = [
    "Lundi",
    "Mardi",
    "Mercredi"
    "Jeudi",
    "Vendredi",
    "Samedi",
    "Dimanche"
    ]

#mock event
event = {
        "start_time": "10:00",
        "end_time": "12:00",
        "description": "Morphic",
        "day": "Lundi",
    }


def create_event(event, week=0):
    current_date = datetime.now().replace(microsecond=0)
    start_week_date = current_date - timedelta(
            days=current_date.weekday(),
            hours=current_date.hour,
            minutes=current_date.minute,
            seconds=current_date.second,
            weeks=week
    )
    start_week_date= start_week_date.replace(microsecond=0)
    end_week_date = start_week_date + timedelta(weeks=1)
    
    i = 0
    for day in days:
        if day == event["day"]:
            break
        i+=1

    start_date = start_week_date + timedelta(days=i, hours=int(event["start_time"][:-3]))
    end_date = start_week_date + timedelta(days=i, hours=int(event["end_time"][:-3]))
    
    query = "INSERT INTO planning(`day`, `description`, `start_time`, `end_time`) VALUES('{}', '{}', '{}', '{}')".format(days[i], event["description"], start_date, end_date)
    
    result = con.execute(query)
    con.commit()
    
    return result

def read_events(week=0):
    current_date = datetime.now().replace(microsecond=0)
    start_week_date = current_date - timedelta(
            days=current_date.weekday(),
            hours=current_date.hour,
            minutes=current_date.minute,
            seconds=current_date.second,
            weeks=week
            )
    start_week_date= start_week_date.replace(microsecond=0)
    end_week_date = start_week_date + timedelta(weeks=1)
    
    query = "SELECT * FROM planning WHERE start_time>date('{}') AND end_time<date('{}')".format(start_week_date, end_week_date)
    results = cur.execute(query)
    
    events = []
    for row in results:
        event["day"] = row[0]
        event["description"] = row[1]
        event["start_time"] = datetime.strptime(row[2], "%Y-%m-%d %H:%M:%S").strftime("%H:%M")
        event["end_time"] = datetime.strptime(row[3], "%Y-%m-%d %H:%M:%S").strftime("%H:%M")
        events.append(event)

    return events


# create a new event
#print(create_event(event))

# read events for current week
print(read_events())

# read events for next week
print(read_events(1))
